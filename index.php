<?php

session_start();

//require( 'php_error.php' );
//\php_error\reportErrors();

const HOME_TAB = 600;
const ENTRY_TAB = 601;
const GRAPH_TAB = 602;

const LOGIN_CONST = 312;
const REGISTER_CONST = 313;

//<editor-fold defaultstate="collapsed" desc="Autoload">

// require the autoload class
require_once("../resources/php/autoload/autoload.class.php");

// Instantiate the autoloader
new Autoload(["php", '../resources/php/']);

//</editor-fold>


$project = new Project();

$project->autoSetup();

$runShell = TRUE;

if(isset($_REQUEST['AJAX']) && $_REQUEST['AJAX'] == TRUE) {
	
	$runShell = FALSE;
	
	if(isset($_REQUEST['file'])) {
		include($_REQUEST['file']);
		die('');
	}
}

if($runShell) {
	
	// get the pages html
	$project->run();
	
}