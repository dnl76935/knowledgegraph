function login(element) {
	
	var username = $('#user').val();
	var password = $('#password').val();
	var email = $('#email').val();
	var loginSelector = $(element).attr("data_id");
	var filename = "./ajax/login.ajax.php";
	var success;
	
	$.ajax({
		'url': 'index.php',
		'type': 'POST',
		async: false,
		'data': {
			file: filename,
			'username': username,
			'password': password,
			'email': email,
			'loginSelector': loginSelector,
			AJAX: true
		},
		success : function(data) {
			success = JSON.parse(data);
			if(success) {
				console.log("successful login");
			}
		},
		error : function(request, error) {
			console.log("error in ajax");
		}
	});
	if(success) {
		window.location.reload();
	}
}

function logout() {
	
	var filename = "./ajax/logout.ajax.php";
	
	$.ajax({
		'url': 'index.php',
		'type': 'POST',
		'data': {
			file: filename,
			AJAX: true
		},
		success : function(data) {
			window.location.reload();
		}
	});
}

function buildRecord() {
	
	var title = $('#title').val();
	var entry = $('#entry').val();
	var tags = getTagString();
	
	if(title.length == 0 || entry.length == 0 || tags.length == 0) {
		alert("You haven't filled out all of the fields!");
	} else {
		
		var filename = "./ajax/build_record.ajax.php";
		
		$.ajax({
			'url': 'index.php',
			'type': 'POST',
			'data': {
				file: filename,
				'title': title,
				'entry': entry,
				'tags': tags,
				AJAX: true
			},
			success : function(data) {
				console.log(data);
			},
			error : function(request, error) {
				console.log("error in ajax");
			}
		});
	}
	
}

function getGraphInfo() {
	
	var filename = "./ajax/build_graph.ajax.php";
	var dataObj = null;
	
	$.ajax({
		'url': 'index.php',
		'type': 'POST',
		async: false,
		'data': {
			file: filename,
			AJAX: true
		},
		success : function(data) {
			
			dataObj = $.parseJSON(data);
			
		},
		error : function(request, error) {
			console.log("error in ajax");
		}
	});
	
	return dataObj;
	
}

function openTab(entryConst) {

	var filename = "./ajax/change_tab.ajax.php";

    $.ajax({
		'url': 'index.php',
		'type': 'POST',
		'data': {
			entryConst: entryConst,
			file: filename,
			AJAX: true
		},
		success : function(data) {
			console.log(data);
		},
		error : function(request, error) {

		}
    });

    window.location.reload();

}

function test() {
	
	var filename = "./ajax/test.ajax.php";
	
	$.ajax({
		'url': 'index.php',
		'type': 'POST',
		'data': {
			file: filename,
			AJAX: true
		},
		success : function(data) {
			
			dataObj = $.parseJSON(data);
			
			console.log(dataObj);
		}
	});
	
}