//function buildGraphs() {
//	
//	var nodeA = {
//			id: 'a'
//	};
//
//	var nodeB = {
//			id: 'b'
//	};
//	
//	var edgeAB = {
//			id: 'ab',
//			source: 'a',
//			target: 'b'
//	};
//	
//	var cy = cytoscape({
//		container: $('#cy'),
////		elements: [
////			{data: nodeA},
////			{data: nodeB},
////			{data: edgeAB}
////		],
//		style: [
//			{
//				selector: 'node',
//				style: {
//					shape: 'hexagon',
//					'background-color': 'red',
//					label: 'data(id)'
//				}
//			}
//		],
//		layout: {
//			name: 'grid'
//		}
//	});
//	cy.add([{data:nodeA}]);
//	cy.add([{data:nodeB}]);
//	cy.add([{data:edgeAB}]);
//	
//}

function buildGraph() {
	
	var graphInfo = getGraphInfo();
	
	var cy = cytoscape({
		container: $('#cy'),
		style: [
			{
				selector: 'node',
				style: {
					shape: 'hexagon',
					'background-color': 'red',
					label: 'data(id)'
				}
			}
		]
	});
	
	for(var i = 0; i < graphInfo.length; i++) {
		var data = graphInfo[i];
		
		cy.add([{
			data: data
		}]);

	}
	
	// var gridLayout = cy.layout({
	// 	name: 'grid'
	// });

	var coseLayout = cy.layout({
		name: 'cose',
		fit: true,
		randomize: false
	});

	// layout.run();

	coseLayout.run();
	
}