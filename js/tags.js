function addTag() {
	
	$("<input/>", {
		type: "text",
		size: "20",
		class: "tagsInput input-sm"
	}).appendTo('#tagsSpan');
	
}

function getTagString() {
	
	var tags = $('.tagsInput');
	var retString = "";
	
	for(var i = 0; i < tags.length; i++) {
		retString += $(tags[i]).val() + ",";
	}
	
	retString = retString.substring(0, retString.length - 1);
	
	return retString;
	
}