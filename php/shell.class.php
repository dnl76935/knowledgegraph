<?php

class Shell extends Shell_V0 {
	
	public function getHtml($title) {
		
		$html = "<!DOCTYPE html> <html>" . $this->getHead($title);
		$body = new Html_Element("body", ["style" => "background-color: lightblue;"]);
		
		if(!isset($_SESSION["user"])) {
			$this->getLoginBody($body);
		} else {
			switch($_SESSION["tab"]) {
				case HOME_TAB:
					$this->getHomeBody($body);
					break;
				case ENTRY_TAB:
					$this->getEntryInputBody($body);
					break;
				case GRAPH_TAB:
					$this->getGraphBody($body);
					break;
				default:
					$this->getHomeBody($body);
			}
		}
		
		$html .= $body . "</html>";
		
		return $html;
		
	}
	
	protected function getHead($title) {
		
		$head = parent::getHead($title);
		
		// compiled and minified bootstrap CSS
		$bootCSS = new Html_Element("link");
		
		$bootCSS->rel = "stylesheet";
		$bootCSS->href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
		$bootCSS->integrity = "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u";
		$bootCSS->crossorigin = "anonymous";
		
		$head->text .= $bootCSS;
		
		// optional bootstrap theme
		$bootOpt = new Html_Element("link");
		
		$bootOpt->rel = "stylesheet";
		$bootOpt->href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css";
		$bootOpt->integrity = "sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp";
		$bootOpt->crossorigin = "anonymous";
		
		$head->text .= $bootOpt;
		
		// compiled and minified JS
		$bootJS = new Html_Element("script");
		
		$bootJS->src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
		$bootJS->integrity = "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa";
		$bootJS->crossorigin = "anonymous";
		
		$head->text .= $bootJS;
		
		return $head;
		
	}
	
	protected function getBody() {

//		$nav = new Navbar();
//		$jumbotron = new Jumbotron();
//
//		$body->text .= $nav->getContainer();
//		$body->text .= $jumbotron->getContainer();
		
	}

	protected function getHomeBody($body) {

        $nav = new Navbar();
        $jumbotron = new Jumbotron();
        $viewerJSthing = new Viewer_JS();

        $body->text .= $nav->getContainer();
        $body->text .= $jumbotron->getContainer();
        $body->text .= $viewerJSthing->getContainer();

    }

	protected function getEntryInputBody($body) {

        $nav = new Navbar();
        $textFileInput = new Text_File_Input();

        $body->text .= $nav->getContainer();
        $body->text .= $textFileInput->getContainer();

    }

    protected function getGraphBody($body) {

        $nav = new Navbar();
        $graph = new Graph_Container();

        $body->text .= $nav->getContainer();
        $body->text .= $graph->getContainer();

    }
	
	protected function getLoginBody($body) {
		
		$loginPage = new Login_Page();
		
		$body->text .= $loginPage->getContainer();
		
	}
	
}