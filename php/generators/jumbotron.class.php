<?php

class Jumbotron {
	
	public function __construct() {
		
		
	}
	
	public function getContainer() {
		
		$jumbo = new Html_Element("div", ["class" => "jumbotron"]);
		
		$container = new Html_Element("div");
		$container->class = "container";
		
		$heading = new Html_Element("h1");
		$heading->class = "display-3";
		$heading->text = "Hello world!";
		
		$displayStuff = new Html_Element();
		$displayStuff->text .= "This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.";
		
		$button = new Html_Element("a");
		$button->class = "btn btn-primary btn-lg";
		$button->role = "button";
		$button->text = "Learn more here!";
		$button->onclick = "$('#entryInputContainer').load('./ajax/test.ajax.php');";
		
		$container->text .= $heading;
		$container->text .= $displayStuff;
		$container->text .= $button;
		
		$jumbo->text .= $container;
		
		return $jumbo;
	}
	
}