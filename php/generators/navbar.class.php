<?php

class Navbar {
	
	
	
	public function __construct() {
		
	}
	
	public function getContainer() {
		
		$container = new Html_Element("div", ["class" => "container navbar-fix"]);
		
		$nav = new Html_Element("nav");
		
		$nav->class = "navbar navbar-inverse navbar-fixed-top bg-inverse";
		
		$this->getNavLinks($nav);
		
		$container->text .= $nav;
		
		return $container;
		
	}
	
	protected function getNavLinks($nav) {
		
		$nav->text .= $this->getHomeLink();
		$nav->text .= $this->getInputLink();
		$nav->text .= $this->getGraphLink();
		$nav->text .= $this->getLogoutLink();
		
	}
	
	protected function getHomeLink() {
		
		$navHome = new Html_Element("a");

		$navHome->class = "navbar-brand clickable";
		$navHome->onclick = "openTab(".HOME_TAB.");";
		$navHome->text .= "Home";
		
		return $navHome;
	}
	
	protected function getInputLink() {
		
		$navInput = new Html_Element("a");

        $navInput->class = "navbar-brand clickable";
		$navInput->onclick = "openTab(".ENTRY_TAB.");";
		$navInput->text .= "Entry Input";
		
		return $navInput;
		
	}
	
	protected function getGraphLink() {
		
		$navGraph = new Html_Element("a");

        $navGraph->class = "navbar-brand clickable";
		$navGraph->onclick = "openTab(".GRAPH_TAB.");";
		$navGraph->text .= "Graph";
		
		return $navGraph;
		
	}
	
	protected function getLogoutLink() {
		
		$navLogout = new Html_Element("a");
		
		$navLogout->class = "navbar-brand navbar-right right-adjusted clickable";
		$navLogout->text .= "Logout";
		$navLogout->onclick = "logout();";
		
		return $navLogout;
		
	}
	
}