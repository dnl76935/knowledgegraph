<?php

class Text_File_Input {
	
	
	
	public function __construct() {
		
		
		
	}
	
	public function getContainer() {
		
		$container = new Html_Element("div", ["id" => "entryInputContainer", "class" => "inputContainer container"]);
		
		$container->text .= $this->getTitleInput();
		$container->text .= $this->getEntryInput();
		$container->text .= $this->getTagsInput();
		$container->text .= new Html_Element("input", ["type" => "submit", "value" => "Submit", "onclick" => "buildRecord()", "class" => "submitEntry btn btn-primary"]);
		
		return $container;
		
	}
	
	protected function getTitleInput() {
		
		$input = new Html_Element("input");
		
		$input->type = "text";
		$input->id = "title";
		$input->size = "30";
		$input->class = "titleInput input-sm";
		
		return $input . "<br>";
		
	}
	
	protected function getEntryInput() {
		
		$input = new Html_Element("textarea");
		
		$input->id = "entry";
		$input->rows = "40";
		$input->cols = "70";
		$input->class = "entryInput input-lg";
		
		return $input . "<br>";
		
	}
	
	protected function getTagsInput() {
		
		$tagsContainer = new Html_Element("span", ["id" => "tagsSpan"]);
		
		$tag = new Html_Element("input");
		
		$tag->type = "text";
		$tag->size = "20";
		$tag->class = "tagsInput input-sm";
		
		$tagsContainer->text .= $tag;
		
		$tagAdder = new Html_Element("button", ["text" => "Click to add tag", "onclick" => "addTag()", "class" => "addTagButton btn"]);
		
		return $tagsContainer . "<br>" . $tagAdder . "<br>";
		
	}
	
}