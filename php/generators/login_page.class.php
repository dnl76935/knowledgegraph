<?php

class Login_Page {
	
	public function __construct() {
		
	}
	
	public function getContainer() {
		
		$container = new Html_Element("div", ["class" => "container"]);
		
		$container->text .= $this->getHeader();
		$container->text .= $this->getRegistrationForm();
		
		return $container;
		
	}
	
	protected function getHeader() {
		
		$jumbo = new Html_Element("div", ["class" => "jumbotron"]);
		
		$heading = new Html_Element("h1");
		$heading->class = "display-3";
		$heading->text = "Register Here!";
		
		$content = new Html_Element();
		$content->text = "You may register here. Pick a non crappy password please or else I can't help you if I get hacked.";
		
		$jumbo->text .= $heading;
		$jumbo->text .= $content;
		
		return $jumbo;
		
	}
	
	protected function getRegistrationForm() {
		
		$form = new Html_Element("form");
		
		$form->text .= $this->getUsernameInput();
		$form->text .= $this->getPasswordInput();
		$form->text .= $this->getEmailInput();
		
		$form->text .= $this->getLoginButton();
		$form->text .= $this->getRegisterButton();
		
		return $form;
		
	}
	
	protected function getUsernameInput() {
		
		$usernameDiv = new Html_Element("div", ["class" => "form-group"]);
		
		$usernameLabel = new Html_Element("label");
		$usernameLabel->for = "user";
		$usernameLabel->text = "Username:";
		
		$usernameInput = new Html_Element("input");
		$usernameInput->type = "text";
		$usernameInput->class = "form-control";
		$usernameInput->id = "user";
		
		$usernameDiv .= $usernameLabel;
		$usernameDiv .= $usernameInput;
		
		return $usernameDiv;
		
	}
	
	protected function getPasswordInput() {
		
		$passwordDiv = new Html_Element("div", ["class" => "form-group"]);
		
		$passwordLabel = new Html_Element("label");
		$passwordLabel->for = "password";
		$passwordLabel->text = "Password:";
		
		$passwordInput = new Html_Element("input");
		$passwordInput->type = "password";
		$passwordInput->class = "form-control";
		$passwordInput->id = "password";
		
		$passwordDiv .= $passwordLabel;
		$passwordDiv .= $passwordInput;
		
		return $passwordDiv;
		
	}
	
	protected function getEmailInput() {
		
		$emailDiv = new Html_Element("div", ["class" => "form-group"]);
		
		$emailLabel = new Html_Element("label");
		$emailLabel->for = "email";
		$emailLabel->text = "Email:";
		
		$emailInput = new Html_Element("input");
		$emailInput->type = "text";
		$emailInput->class = "form-control";
		$emailInput->id = "email";
		
		$emailDiv .= $emailLabel;
		$emailDiv .= $emailInput;
		
		return $emailDiv;
		
	}
	
	protected function getLoginButton() {
		
		$button = new Html_Element("button");
		$button->class = "btn btn-primary btn-lg centered";
		$button->role = "button";
		$button->onclick = "login(this);";
		$button->text = "Login";
		$button->data_id = "login";
		
		return $button;
		
	}
	
	protected function getRegisterButton() {

		$button = new Html_Element("button");
		$button->class = "btn btn-primary btn-lg centered";
		$button->role = "button";
		$button->onclick = "login(this);";
		$button->text = "Register";
		$button->data_id = "register";
		
		return $button;
	}
	
}