<?php

class Login_Handler {
	
	protected $username;
	protected $password;
	protected $email;
	protected $loginSelector;
	
	public function __construct($username, $password, $email, $loginSelector) {
		
		$this->username = $username;
		$this->password = $password;
		$this->email = $email;
		$this->loginSelector = $loginSelector;
		
	}
	
	public function moveToSite() {
		
		$loginSuccess;
		
		if($this->loginSelector == LOGIN_CONST) {
			$loginSuccess = $this->login();
		} else {
			$loginSuccess = $this->register();
		}
		
		return $loginSuccess;
		
	}
	
	protected function login() {
		
		$hasAccount = false;
		$passwordMatches = false;
		
		$mysql = "SELECT username FROM members";
		
		$results = Mysql_Obj::runSelectQuery($mysql);
		
		if(sizeof($results) > 0) {
			foreach($results as $result) {
				if(strcmp($result["username"], $this->username) == 0) {
					$hasAccount = true;
				}
			}
		}
		
		if($hasAccount) {
			$mysql = "SELECT password FROM members WHERE username = \"$this->username\"";

			$results = Mysql_Obj::runSelectQuery($mysql);
			
			$userPassword = $results[0]["password"];
			
			if(password_verify($this->password, $userPassword)) {
				$passwordMatches = true;
			}
			
		}
		
		if($hasAccount && $passwordMatches) {
			$_SESSION["user"] = $this->username;
			$_SESSION["tab"] = HOME_TAB;
			return true;
		} else {
			// do something because username doesn't exist or password doesn't match one on file
			return false;
		}
		
	}
	
	protected function register() {
		
		$accountExists = false;
		
		$mysql = "SELECT username FROM members";
		
		$results = Mysql_Obj::runSelectQuery($mysql);
		
		if(sizeof($results) > 0) {
			foreach($results as $result) {
				if(strcmp($result["username"], $this->username) == 0) {
					$accountExists = true;
				}
			}
		}

		if($accountExists) {
			return false;
		} else {
			$hashedPassword = password_hash($this->password, PASSWORD_DEFAULT);
			$mysql = "INSERT INTO members (username, email, password) VALUES (\"$this->username\", \"$this->email\", \"$hashedPassword\")";
			
			$result = Mysql_Obj::runInsertQuery($mysql);
			$success = $result->getValid();
			
			$_SESSION["user"] = $this->username;

			return $success;
		}
		
	}
	
}