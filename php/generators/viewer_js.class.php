<?php
/**
 * Created by PhpStorm.
 * User: dnl76935
 * Date: 6/8/2017
 * Time: 1:49 PM
 */
class Viewer_JS {

    public function __construct() {

    }

    public function getContainer() {

        $container = new Html_Element("div");

        $preview = new Html_Element("iframe");
        
        $url = urlencode("http://knowledgegraph.broke-it.net/sandboxes/dj/resources/word_doc/Geog_360-Lecture_2and3_Film_The_East_pt1_2017-06-08_111332.docx");

        $preview->src = "https://view.officeapps.live.com/op/view.aspx?src=$url";
//         $preview->text = print_r(scandir("../ViewerJS/"));

        $container->text .= $preview;

        return $container;

    }

}