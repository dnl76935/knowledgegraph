<?php

class Project {

    protected $directories = [
        "./php",
        "../resources/db",
        "../resources/php"
    ];

	public function __construct() {
		
	}
	
	public function run() {
		
		$shell = new Shell();
		
		$html = $shell->getHtml("KnoGraph");
		
		echo $html;
		
	}

    public function autoSetup() {
        // setup error reporting
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }
	
}