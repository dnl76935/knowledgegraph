<?php

$username = $_POST["username"];
$password = $_POST["password"];
$email = $_POST["email"];
$loginSelector = $_POST["loginSelector"];

$loginSelector = (strcmp($loginSelector, "login") == 0 ? LOGIN_CONST : REGISTER_CONST);

$loginHandler = new Login_Handler($username, $password, $email, $loginSelector);

$success = $loginHandler->moveToSite();

echo json_encode($success);