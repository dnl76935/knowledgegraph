<?php

$mysql = "SELECT id FROM entry_item";

$ids = Mysql_Obj::runSelectQuery($mysql);

$entryItems = array();
$nodes = array();
$edges = array();

foreach($ids as $id) {
	$entryItems[] = Entry_Item::getById($id["id"]);
}

foreach($entryItems as $item) {
	$nodes[] = new Cytoscape_Node($item->getTitle());
}

foreach($entryItems as $item1) {
	$item1tags = $item1->getTags();
	foreach($entryItems as $item2) {
		$item2tags = $item2->getTags();
		foreach($item1tags as $tag1) {
			foreach($item2tags as $tag2) {
				if(strcmp($tag1, $tag2) == 0 && strcmp($item1->getTitle(), $item2->getTitle()) != 0) {
					$edges[] = new Cytoscape_Edge($item1->getTitle() . "|" . $item2->getTitle(), $item1->getTitle(), $item2->getTitle());
				}
			}
		}
	}
}

$cytoscapeElements = new Cytoscape_Elements();

foreach($nodes as $node) {
	$cytoscapeElements->addNodeOrEdge($node->getData());
}
foreach($edges as $edge) {
	$cytoscapeElements->addNodeOrEdge($edge->getData());
}

echo json_encode($cytoscapeElements->getNetwork());